package com.meshgroup.web.controller;

import com.meshgroup.domain.Error;
import com.meshgroup.service.ErrorService;
import com.meshgroup.service.ProfileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {
    private ErrorService errorService;

    @Autowired
    public void setErrorService(ErrorService errorService) {
        this.errorService = errorService;
    }

    @ExceptionHandler(ProfileException.class)
    public ResponseEntity<String> sendException(ProfileException exception) {
        Error error = exception.getError();
        errorService.save(error);
        return new ResponseEntity<>(getJsonMessage(error), error.getStatus());
    }

    private String getJsonMessage(Error error) {
        return "{message: " + error.getMessage() + " }";
    }
}