package com.meshgroup.web.controller;

import com.meshgroup.domain.Profile;
import com.meshgroup.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/profiles")
public class ProfileController {
    private ProfileService profileService;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping(value = "/set")
    public int setProfile(@RequestAttribute("profile") Profile profile) {
        return profileService.save(profile);
    }

    @GetMapping("/last")
    public Profile getLastProfile() {
        return profileService.findLast();
    }

    @GetMapping
    public List<Profile> getProfiles() {
        return profileService.findProfiles();
    }

    @GetMapping("/{id}")
    public Profile getById(@PathVariable("id") int id) {
        return profileService.findById(id);
    }

    @PostMapping("/get")
    public Profile setProfile(@RequestParam("email") String email) {
        return profileService.findByEmail(email);
    }
}