package com.meshgroup.web.controller;

import com.meshgroup.domain.Error;
import com.meshgroup.service.ErrorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "whit EXCEPTION occurs") // in swagger replaces controller name by this value
public class ErrorController {
    private ErrorService errorService;

    @Autowired
    public void setErrorService(ErrorService errorService) {
        this.errorService = errorService;
    }

    @ApiOperation(value = "Returns last exception") // in swagger replaces method name by this value
    @ApiResponses(                                  // in swagger adds response statuses
            value = {@ApiResponse(code = 100, message = "response message with status 100"),
                    @ApiResponse(code = 200, message = "success message")})
    @GetMapping("/error/last")
    public Error getLast() {
        return errorService.findLast();
    }
}