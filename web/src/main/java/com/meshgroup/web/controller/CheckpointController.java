package com.meshgroup.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static java.util.Collections.singletonMap;

@Controller
public class CheckpointController {
    @Value("${secret.key}")
    private String secretKey;

    @GetMapping("/exit-success")
    public String forwardToSuccessLogoutPage() {
        return "/static/logout.html";
    }

    @PostMapping(value = "/", produces = "application/json")
    @ResponseBody
    public Map<String, String> getAccess(HttpServletResponse response) {
        response.addCookie(new Cookie("secret", secretKey));
        return singletonMap("msg", "Now you can do requests");
    }
}