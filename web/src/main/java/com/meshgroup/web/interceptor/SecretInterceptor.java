package com.meshgroup.web.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.web.util.WebUtils.getCookie;

@Component
public class SecretInterceptor extends HandlerInterceptorAdapter {
    @Value("${secret.key}")
    private String token;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String secretCookie = getSecretCookie(request);
        if (token.equals(secretCookie)) {
            return true;
        } else {
            response.getWriter().write("{\"message\": \"There is no secret token or it is not correct\"}");
            response.setStatus(401);
            return false;
        }
    }

    private String getSecretCookie(HttpServletRequest request) {
        Cookie secretCookie = getCookie(request, "secretCookie");
        return secretCookie != null ? secretCookie.getValue() : null;
    }
}