package com.meshgroup.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshgroup.domain.Profile;
import com.meshgroup.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;

public class EmailValidatorFilter extends OncePerRequestFilter {
    private static final String EMAIL_EXPRESSION = "^(.+)@(.+)$";
    private ProfileService profileService;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String email = getEmail(httpServletRequest, httpServletResponse);
        if (isEmailValid(email)) {
            if (!profileService.isEmailExists(email)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } else {
                httpServletResponse.setStatus(FORBIDDEN.value());
                httpServletResponse.getWriter().write(getJsonMessage("Such email exists"));
            }
        } else {
            if (httpServletResponse.getBufferSize() == 0) {
                httpServletResponse.setStatus(BAD_REQUEST.value());
                httpServletResponse.getWriter().write(getJsonMessage("Email is wrong"));
            }
        }
    }

    private String getEmail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Profile profile = mapper.readValue(request.getInputStream(), Profile.class);
            request.setAttribute("profile", profile);
            return profile.getEmail();
        } catch (IOException e) {
            response.setStatus(BAD_REQUEST.value());
            response.getWriter().write(getJsonMessage("Request has not expected json values"));
        }
        return null;
    }

    private String getJsonMessage(String text) {
        return "{\"message\": \"" + text + "\"}";
    }

    private boolean isEmailValid(String email) {
        return email != null && email.matches(EMAIL_EXPRESSION);
    }
}