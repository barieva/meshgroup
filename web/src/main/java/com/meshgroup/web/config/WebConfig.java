package com.meshgroup.web.config;

import com.meshgroup.service.ServiceConfig;
import com.meshgroup.web.filter.EmailValidatorFilter;
import com.meshgroup.web.interceptor.SecretInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@Import(ServiceConfig.class)
@EnableSwagger2
@ComponentScan(basePackages = "com.meshgroup.web")
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private SecretInterceptor secretInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(secretInterceptor).addPathPatterns("/profiles*");
    }

    @Bean
    public FilterRegistrationBean<EmailValidatorFilter> registrationBean() {
        FilterRegistrationBean<EmailValidatorFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(emailValidatorFilter());
        registrationBean.addUrlPatterns("/profiles/set");
        registrationBean.setName("emailValidatorFilter");
        return registrationBean;
    }

    @Bean
    public EmailValidatorFilter emailValidatorFilter(){
        return new EmailValidatorFilter();
    }


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.meshgroup.web"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {
        return new ApiInfo(
                "Spring Boot Swagger Example API",
                "Spring Boot Swagger Example API for Mesh Group",
                "1.0",
                "Terms of Service",
                new Contact("getJavaJob", "http://www.getjavajob.com/",
                        ""),
                "Apache License Version 2.0",
                "https://www.apache.org/licesen.html"
        );
    }
}