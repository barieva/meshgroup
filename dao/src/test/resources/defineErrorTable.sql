create table if not exists Error
(
    id      serial not null primary key,
    message varchar(255),
    status  varchar(255),
    created timestamp
);

INSERT INTO Error (id, message, status, created)
VALUES (1, 'message1', 'NOT_FOUND', '2020-01-01'),
       (2, 'message2', 'METHOD_NOT_ALLOWED', '2020-02-02'),
       (3, 'message3', 'NOT_ACCEPTABLE', '2018-03-03'),
       (4, 'message4', 'PROXY_AUTHENTICATION_REQUIRED', '2019-04-04'),
       (5, 'message5', 'REQUEST_TIMEOUT', '2020-05-05');