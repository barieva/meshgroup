create table if not exists Profile
(
    id      serial       not null primary key,
    name    varchar(255),
    email   varchar(255) not null,
    age     integer,
    created timestamp
);

create unique index if not exists profile_email_uindex
    on profile (email);
ALTER TABLE profile
    ALTER COLUMN email SET NOT NULL;
INSERT INTO profile (id, name, email, age, created)
VALUES (1, 'profile1', 'email1', 10, '2020-01-01'),
       (2, 'profile2', 'email2', 20, '2020-02-02'),
       (3, 'profile3', 'email3', 30, '2018-03-03'),
       (4, 'profile4', 'email4', 40, '2019-04-04'),
       (5, 'profile5', 'email5', 50, '2020-05-05');