package com.meshgroup.dao;

import com.meshgroup.domain.Error;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ConfigDaoTest.class)
@Sql(scripts = {"/defineErrorTable.sql"})
@Sql(scripts = {"/truncateErrorTable.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
class ErrorDaoTest {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ErrorDao errorDao;

    @Test
    void saveProfileWithEmailAndNameLowerCase() {
        Error error = new Error();
        error.setMessage("text");
        error.setStatus(NOT_FOUND);
        int savedProfileId = errorDao.save(error);
        pushChangesToDb();
        assertEquals(6, savedProfileId, "New inserted error has to be with 6 id");
    }

    @Test
    void saveNullError() {
        assertThrows(IllegalArgumentException.class, () -> errorDao.save(null));
    }

    @Test
    void saveEmptyError() {
        assertEquals(6, errorDao.save(new Error()), "Empty error allowed in db");
    }

    @Test
    void findLast() {
        Error error = new Error();
        error.setId(5);
        assertEquals(error, errorDao.findLast(), "The latest entity have to be with id 5");
    }

    @Test
    void findLastFromEmptyTable() {
        entityManager.createQuery("delete from Error").executeUpdate();
        pushChangesToDb();
        assertNull(errorDao.findLast(), "There is no last error in empty table");
    }

    private void pushChangesToDb() {
        entityManager.flush();
        entityManager.clear();
    }
}