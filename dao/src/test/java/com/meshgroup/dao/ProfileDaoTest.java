package com.meshgroup.dao;

import com.meshgroup.domain.Profile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ConfigDaoTest.class)
@Sql(scripts = {"/defineProfileTable.sql"})
@Sql(scripts = {"/truncateProfileTable.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
class ProfileDaoTest {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ProfileDao profileDao;

    @BeforeEach
    public void setEntityManager() {
        profileDao.setEntityManager(entityManager);
    }

    @Test
    void findByEmail() {
        assertEquals(new Profile(1), profileDao.findByEmail("email1"), "Profile with id = 1 by email = email1 expected");
    }

    @Test
    void findByNonexistentEmail() {
        assertNull(profileDao.findByEmail("NONEXISTENT"), "Email not exists in db but method returned not null value");
    }

    @Test
    void findByNullEmail() {
        assertNull(profileDao.findByEmail(null), "Email not exists in db but method returned not null value");
    }

    @Test
    void findById() {
        Profile profile = new Profile();
        profile.setId(5);
        assertEquals(profile, profileDao.findById(5), "Profile with id = 5 by id = 5 expected");
    }

    @Test
    void findByNonexistentId() {
        assertNull(profileDao.findById(999999999), "Id not exists in db but method returned not null value");
    }

    @Test
    void findAll() {
        List<Profile> profiles = asList(
                new Profile(1),
                new Profile(2),
                new Profile(3),
                new Profile(4),
                new Profile(5));
        assertEquals(profiles, profileDao.findAll(), "Table has not 5 entities as in the beginning");
    }

    @Test
    void findAllFromEmptyTable() {
        List<Profile> profiles = emptyList();
        entityManager.createQuery("delete from Profile").executeUpdate();
        pushChangesToDb();
        assertEquals(profiles, profileDao.findAll(), "Table is not empty");
    }

    @Test
    void findLast() {
        assertEquals(new Profile(5), profileDao.findLast(), "The latest entity have to be with id 5");
    }

    @Test
    void findLastFromEmptyTable() {
        entityManager.createQuery("delete from Profile").executeUpdate();
        pushChangesToDb();
        assertNull(profileDao.findLast(), "There is no last entity in empty table");
    }

    @Test
    void saveProfileWithEmailAndNameLowerCase() {
        Profile profile = getProfile();
        int savedProfileId = profileDao.save(profile);
        pushChangesToDb();
        assertEquals(6, savedProfileId, "New inserted entity has to be with 6 id");
    }

    @Test
    void saveProfileWithEmailAndNameUpperCase() {
        Profile profile = getProfile();
        profile.setName("UPPER_NAME");
        profile.setEmail("UPPER_EMAIL");
        int savedProfileId = profileDao.save(profile);
        pushChangesToDb();
        assertEquals(6, savedProfileId, "New inserted entity has to be with 6 id");
    }

    @Test
    void saveProfileWithExistentEmail() {
        Profile profile = getProfile();
        profile.setEmail("email1");
        assertThrows(PersistenceException.class, () -> profileDao.save(profile));
    }

    @Test
    void saveNullProfile() {
        assertThrows(IllegalArgumentException.class, () -> profileDao.save(null));
    }

    @Test
    void saveEmptyProfile() {
        Profile profile = new Profile();
        assertThrows(PersistenceException.class, () -> profileDao.save(profile));
    }

    @Test
    void testExistsByEmail() {
        assertEquals(true, profileDao.existsByEmail("email1"), "'Email1' is in db but returned false");
    }

    @Test
    void testNotExistsByEmail() {
        assertEquals(false, profileDao.existsByEmail("nonexistent"), "'Nonexistent' is not in db but returned true");
    }

    @Test
    void testExistsByEmailByNull() {
        assertEquals(false, profileDao.existsByEmail(null), "null is not in db but returned true");
    }

    private Profile getProfile() {
        Profile profile = new Profile();
        profile.setEmail("email6");
        profile.setCreated(now());
        profile.setName("Name6");
        return profile;
    }

    private void pushChangesToDb() {
        entityManager.flush();
        entityManager.clear();
    }
}