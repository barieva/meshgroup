package com.meshgroup.dao;

import com.meshgroup.domain.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ProfileDao {
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Profile findByEmail(String email) {
        try {
            Query query = entityManager.createQuery("FROM Profile WHERE email =:email").setParameter("email", email);
            query.setHint("org.hibernate.cacheable", true);
            return (Profile) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Boolean existsByEmail(String email) {
        try {
            Query query = entityManager.createQuery("select 1 from Profile where email = :email").setParameter("email", email);
            query.setHint("org.hibernate.cacheable", true);
            return query.getSingleResult() != null;
        } catch (NoResultException e) {
            return false;
        }
    }

    public Profile findById(int id) {
        try {
            return entityManager.find(Profile.class, id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Profile> findAll() {
        Query query = entityManager.createQuery("FROM Profile");
        query.setHint("org.hibernate.cacheable", true);
        return query.getResultList();
    }

    public Profile findLast() {
        try {
            Query query = entityManager.createQuery("FROM Profile order by id desc").setMaxResults(1);
            query.setHint("org.hibernate.cacheable", true);
            return (Profile) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public int save(Profile profile) {
        entityManager.persist(profile);
        return profile.getId();
    }
}