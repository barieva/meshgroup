package com.meshgroup.dao;

import com.meshgroup.domain.Error;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class ErrorDao {
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public int save(Error error) {
        entityManager.persist(error);
        return error.getId();
    }

    public Error findLast() {
        try {
            Query query = entityManager.createQuery("FROM Error order by id desc").setMaxResults(1);
            query.setHint("org.hibernate.cacheable", true);
            return (Error) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}