package com.meshgroup.service;

import com.meshgroup.dao.ErrorDao;
import com.meshgroup.domain.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class ErrorService {
    ErrorDao errorDao;

    @Autowired
    public void setErrorDao(ErrorDao errorDao) {
        this.errorDao = errorDao;
    }

    @Transactional
    public int save(Error error) {
        return errorDao.save(error);
    }

    @Transactional(readOnly = true)
    public Error findLast() {
        Error error = errorDao.findLast();
        if (error != null) {
            return error;
        } else {
            Error newError = new Error();
            newError.setStatus(NOT_FOUND);
            newError.setMessage("There is no error in db");
            throw new ProfileException(newError);
        }
    }
}