package com.meshgroup.service;

import com.meshgroup.domain.Error;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileException extends RuntimeException {
    private Error error;

    public ProfileException(Error error) {
        this.error = error;
    }
}