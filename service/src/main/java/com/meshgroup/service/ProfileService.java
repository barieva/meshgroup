package com.meshgroup.service;

import com.meshgroup.dao.ProfileDao;
import com.meshgroup.domain.Error;
import com.meshgroup.domain.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.time.LocalDateTime.now;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class ProfileService {
    private ProfileDao profileDao;

    @Autowired
    public void setProfileDao(ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    @Transactional
    public int save(Profile profile) {
        profile.setCreated(now());
        return profileDao.save(profile);
    }

    @Transactional(readOnly = true)
    public boolean isEmailExists(String email) {
        return profileDao.existsByEmail(email);
    }

    @Transactional(readOnly = true)
    public Profile findLast() {
        Profile profile = profileDao.findLast();
        if (profile != null) {
            return profile;
        } else {
            Error error = new Error();
            error.setStatus(NOT_FOUND);
            error.setMessage("There is no profile in db");
            throw new ProfileException(error);
        }
    }

    @Transactional(readOnly = true)
    public List<Profile> findProfiles() {
        return profileDao.findAll();
    }

    @Transactional(readOnly = true)
    public Profile findById(int id) {
        Profile profile = profileDao.findById(id);
        if (profile != null) {
            return profile;
        } else {
            Error error = new Error();
            error.setStatus(NOT_FOUND);
            error.setMessage("There is no profile by such id");
            throw new ProfileException(error);
        }
    }

    @Transactional(readOnly = true)
    public Profile findByEmail(String email) {
        Profile profile = profileDao.findByEmail(email);
        if (profile != null) {
            return profile;
        } else {
            Error error = new Error();
            error.setStatus(NOT_FOUND);
            error.setMessage("There is no profile by such email");
            throw new ProfileException(error);
        }
    }
}