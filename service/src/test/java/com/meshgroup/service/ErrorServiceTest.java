package com.meshgroup.service;

import com.meshgroup.dao.ErrorDao;
import com.meshgroup.domain.Error;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ExtendWith(SpringExtension.class)
class ErrorServiceTest {
    @Mock
    ErrorDao errorDao;
    @InjectMocks
    ErrorService errorService;

    @Test
    void saveError() {
        Error error = new Error();
        error.setMessage("text");
        error.setStatus(NOT_FOUND);
        when(errorDao.save(error)).thenReturn(1);
        assertEquals(1, errorService.save(error), "Expected id = 1");
    }

    @Test
    void saveNullError() {
        when(errorDao.save(null)).thenThrow(PersistenceException.class);
        assertThrows(PersistenceException.class, () -> errorService.save(null));
    }

    @Test
    void saveEmptyProfile() {
        Error emptyError = new Error();
        emptyError.setId(1);
        when(errorDao.save(emptyError)).thenReturn(1);
        assertEquals(1, errorDao.save(emptyError), "Empty error allowed in db");
    }

    @Test
    void findLastError() {
        Error error = new Error();
        error.setMessage("text");
        error.setStatus(NOT_FOUND);
        when(errorDao.findLast()).thenReturn(error);
        assertEquals(error, errorService.findLast(), "Last entity not returned");
    }

    @Test
    void findNonexistentLastError() {
        when(errorDao.findLast()).thenReturn(null);
        assertThrows(ProfileException.class, () -> errorService.findLast());
    }
}