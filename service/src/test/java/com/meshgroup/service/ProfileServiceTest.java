package com.meshgroup.service;

import com.meshgroup.dao.ProfileDao;
import com.meshgroup.domain.Profile;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.PersistenceException;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ProfileServiceTest {
    @Mock
    ProfileDao profileDao;
    @InjectMocks
    ProfileService profileService;

    @Test
    void saveProfileWithEmailAndNameLowerCase() {
        Profile profile = new Profile();
        profile.setEmail("email");
        profile.setName("name");
        profile.setCreated(LocalDateTime.now());
        when(profileDao.save(profile)).thenReturn(1);
        assertEquals(1, profileService.save(profile), "Expected id = 1");
    }

    @Test
    void saveProfileWithEmailAndNameUpperCase() {
        Profile profile = new Profile();
        profile.setEmail("EMAIL");
        profile.setName("NAME");
        profile.setCreated(LocalDateTime.now());
        when(profileDao.save(profile)).thenReturn(1);
        assertEquals(1, profileService.save(profile), "Expected id = 1");
    }

    @Test
    void saveProfileWithExistentEmail() {
        Profile profile = new Profile();
        profile.setEmail("sameEmailInDb");
        when(profileDao.save(profile)).thenThrow(PersistenceException.class);
        assertThrows(PersistenceException.class, () -> profileService.save(profile));
    }

    @Test
    void saveNullProfile() {
        when(profileDao.save(null)).thenThrow(PersistenceException.class);
        assertThrows(PersistenceException.class, () -> profileService.save(null));
    }

    @Test
    void saveEmptyProfile() {
        Profile profile = new Profile();
        when(profileDao.save(profile)).thenThrow(PersistenceException.class);
        assertThrows(PersistenceException.class, () -> profileDao.save(profile));
    }

    @Test
    void testExistsByEmail() {
        when(profileDao.existsByEmail("email1")).thenReturn(true);
        assertEquals(true, profileService.isEmailExists("email1"), "'email1' is in db but returned false");
    }

    @Test
    void testNotExistsByEmail() {
        when(profileDao.existsByEmail("nonexistent")).thenReturn(false);
        assertEquals(false, profileService.isEmailExists("nonexistent"), "'Nonexistent' is not in db but returned true");
    }

    @Test
    void testExistsByEmailByNull() {
        when(profileDao.existsByEmail(null)).thenReturn(false);
        assertEquals(false, profileService.isEmailExists(null), "null is not in db but returned true");
    }

    @Test
    void findLastProfile() {
        Profile profile = new Profile(1);
        profile.setEmail("email");
        profile.setName("name");
        profile.setCreated(LocalDateTime.now());
        profile.setAge(1);
        when(profileDao.findLast()).thenReturn(profile);
        assertEquals(profile, profileService.findLast(), "Last entity not returned");
    }

    @Test
    void findNonexistentLastProfile() {
        when(profileDao.findLast()).thenReturn(null);
        assertThrows(ProfileException.class, () -> profileService.findLast());
    }

    @Test
    void findProfiles() {
        List<Profile> profiles = asList(new Profile(1), new Profile(2));
        when(profileDao.findAll()).thenReturn(profiles);
        assertEquals(profiles, profileService.findProfiles(), "There is no expected profiles");
    }

    @Test
    void findProfilesFromEmptyTable() {
        List<Profile> profiles = emptyList();
        when(profileDao.findAll()).thenReturn(profiles);
        assertEquals(profiles, profileService.findProfiles(), "Expected empty list");
    }

    @Test
    void findById() {
        Profile profile = new Profile();
        profile.setId(5);
        when(profileDao.findById(5)).thenReturn(profile);
        assertEquals(profile, profileService.findById(5), "Expected profile with id 5");
    }

    @Test
    void findByNonexistentId() {
        when(profileDao.findById(999999999)).thenReturn(null);
        assertThrows(ProfileException.class, () -> profileService.findById(999999999));
    }

    @Test
    void findByEmail() {
        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("email1");
        when(profileDao.findByEmail("email1")).thenReturn(profile);
        assertEquals(profile, profileService.findByEmail("email1"), "Profile with id = 1 by email = email1 expected");
    }

    @Test
    void findByNonexistentEmail() {
        when(profileDao.findByEmail("NONEXISTENT")).thenReturn(null);
        assertThrows(ProfileException.class, () -> profileService.findByEmail("NONEXISTENT"));
    }

    @Test
    void findByNullEmail() {
        when(profileDao.findByEmail(null)).thenReturn(null);
        assertThrows(ProfileException.class, () -> profileService.findByEmail(null));
    }
}